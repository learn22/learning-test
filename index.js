import {AppRegistry} from 'react-native';
import Navigation from './app/Navigation';
import {name as appName} from './app.json';
//import App from './app/d/App';

AppRegistry.registerComponent(appName, () => Navigation);
