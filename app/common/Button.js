import React from 'react';
import {Button, TouchableOpacity} from 'react-native';

const ButtonComponent = props => {
  return (
    <TouchableOpacity>
      <Button title={props.children} onPress={props.onPress} />
    </TouchableOpacity>
  );
};

export default ButtonComponent;
