import React from 'react';
import {View, StyleSheet, Text} from 'react-native';
import {TextInput} from 'react-native-gesture-handler';

const MyInput = props => {
  return (
    <View style={Styles.inputContainer}>
      <Text style={Styles.lebel}>{props.label}</Text>
    </View>
  );
};

const Styles = StyleSheet.create({
  inputContainer: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    height: 40,
  },

  lebel: {
    fontSize: 16,
    paddingLeft: 20,
    flex: 1,
  },

  input: {
    fontSize: 16,
    color: '#000',
    paddingLeft: 5,
    paddingRight: 5,
    flex: 2,
  },
});

export {MyInput};

/*
<TextInput
  placeholder={props.placeholder}
  secureTextEntry={props.secureTextEntry}
  autoCorrect={false}
  onChangeText={props.onChangeText}
  style={Styles.input}
/>
*/
