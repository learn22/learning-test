import React, {Component} from 'react';
import {View, StyleSheet} from 'react-native';
import ButtonComponent from './common/Button';

const styles = StyleSheet.create({
  default: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});

class Home extends Component {
  _loginButton() {
    this.props.navigation.navigate('Login');
  }

  _contactUsButton() {
    this.props.navigation.navigate('Contact Us');
  }

  render() {
    return (
      <View style={{flex: 1}}>
        <View style={styles.default}>
          <ButtonComponent onPress={this._loginButton.bind(this)}>Login to app</ButtonComponent>
        </View>

        <View style={styles.default}>
          <ButtonComponent onPress={this._contactUsButton.bind(this)}>Contact Us</ButtonComponent>
        </View>
      </View>
    );
  }
}
export default Home;
