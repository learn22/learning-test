import React, {Component} from 'react';
import {View, Button, StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  default: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});

function Profile({navigation}) {
  return (
    <View style={styles.default}>
      <Button title="Go back" onPress={() => navigation.goBack()} />
    </View>
  );
}

export default Profile;
