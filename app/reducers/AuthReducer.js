//import {State} from 'react-native-gesture-handler';

const INITIAL_STAT = {user: null, load: false, error: 'null'};

export default (state = INITIAL_STAT, action) => {
  console.log(action.type);
  switch (action.type) {
    case 'login_attempt':
      return {...state, load: true, error: 'true'};
    default:
      return state;
  }
};
