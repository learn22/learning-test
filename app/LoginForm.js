import React, {Component} from 'react';
import {View, Button, StyleSheet} from 'react-native';
import {CardItem, Card, MyInput, Spinner} from './common';
import {TextInput} from 'react-native-gesture-handler';
import {loginAction} from './actions';
import {connect} from 'react-redux';
//import { State } from 'react-native-gesture-handler';

const Styles = StyleSheet.create({
  input: {
    fontSize: 16,
    color: '#000',
    paddingLeft: 5,
    paddingRight: 5,
    flex: 2,
  },
});

class LoginForm extends Component {
  constructor() {
    super();
    this.state = {
      email: '',
      password: '',
    };
  }

  _appLogin() {
    //console.log(this.state.email);
    //console.log(this.state.password);

    const {email, password} = this.state;
    loginAction({email, password});
  }

  _renderButton() {
    console.log(this.props.error);
    if (this.props.load) {
      return <Spinner />;
    }
    return <Button title="Login" onPress={this._appLogin.bind(this)} />;
  }

  render() {
    return (
      <View>
        <Card>
          <CardItem>
            <MyInput label="Email" />
            <TextInput
              placeholder={'Enter the email!'}
              onChangeText={email => this.setState({email})}
              style={Styles.input}
              secureTextEntry={false}
            />
          </CardItem>
          <CardItem>
            <MyInput label="Password" />
            <TextInput
              placeholder={'Enter the pass!'}
              onChangeText={password => this.setState({password})}
              style={Styles.input}
              secureTextEntry={true}
            />
          </CardItem>
          <CardItem>{this._renderButton()}</CardItem>
        </Card>
      </View>
    );
  }
}

const mapStateToProps = state => {
  return {
    error: state.auth.error,
    loading: state.auth.loading,
    user: state.auth.user,
  };
};

export default connect(
  mapStateToProps,
  loginAction,
)(LoginForm);
