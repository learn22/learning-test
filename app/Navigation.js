import {createStore} from 'redux';
import {Provider} from 'react-redux';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import React from 'react';
import ContactUs from './ContactUs';
import LoginForm from './LoginForm';
import Home from './Home';

import reducers from './reducers';

const Stack = createStackNavigator();

function MyStack() {
  return (
    <Stack.Navigator>
      <Stack.Screen name="My home" component={Home} />
      <Stack.Screen name="Login" component={LoginForm} />
      <Stack.Screen name="Contact Us" component={ContactUs} />
    </Stack.Navigator>
  );
}

const Navigation = () => {
  return (
    <Provider store={createStore(reducers)}>
      <NavigationContainer>
        <MyStack />
      </NavigationContainer>
    </Provider>
  );
};

export default Navigation;
